﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using coreLib;

namespace CommandLineOS
{
    class Program
    {
        static void exitStatement()
        {
            Console.Write("Press any key contiune to main menu......"); //输出辅助信息
            Console.ReadKey(); //等待按键
            mainMenu(); //返回主界面
        }
        static Process newThread(string filename)
        { 
            //新线程代码
            Process thread = new Process(); //声明新线程
            thread.StartInfo.FileName = filename; //设置执行的文件名
            return thread; //返回线程对象而不启动线程
        }
        static void callApp(string filename)
        {
            Process thread = newThread(filename);
            thread.Start();
            return;
        }
        static int mainMenu()
        {
            // 变量/属性声明区
            string vercode = "0.1 1102Beta";

            //输出菜单
            Console.Clear();
            //输出辅助信息
            Console.WriteLine("Function Menu:");
            Console.WriteLine(" 1 : Advanced Calculator");
            Console.WriteLine(" 2 : System Version");
            Console.Write("Please enter Application Number:>");
            string choice = Console.ReadLine(); //接受输入
            switch(choice)
            { 
                
                case "1": //高级计算器
                    Console.ReadKey();
                    callApp("Advanced_Calculator.exe"); //调用应用程序
                    exitStatement(); //退出
                    break;

                case "2": //系统版本
                    Console.WriteLine("Current version is :" + vercode);
                    callApp("about.exe"); //调用易语言编写的关于窗口（图形界面）
                    exitStatement(); //退出
                    break;

                default:
                    //输出辅助信息
                    Console.WriteLine("Enter a Unknow Application Number......");
                    mainMenu();
                    break;
            }
            
            return 0;
        }
        
        static int LoginAPI(string uid, string pwd)
        {
            //User Login System
            //Judgement UserID Right - 判断用户ID是否正确
            if (uid == "admin")
            {
                ;
            }
            else 
            { 
                //Incorrect Password Statement
                Console.WriteLine("You enter's Password is incorrect!");
                Console.WriteLine("Login Denid!");
                Console.WriteLine("Please Re Login!");
                LoginUI();
            }
            if (pwd == "master")
            {
                //Judgement User Password Right - 判断用户密码是否正确
                return 1;
            }
            else
            {
                //Input a wrong User Password
                return 0;
            }
        }

        static int LoginUI() 
        {
            Console.WriteLine("Please enter your User ID:>");
            string uid = Console.ReadLine();
            Console.WriteLine("Please enter you Password:>");
            string pwd = Console.ReadLine();
            
            int a = LoginAPI(uid, pwd);
            if (a == 1) 
            {
                Console.Clear();
                Console.WriteLine("Login success!");
                Console.WriteLine("Entering Main Interface Now......");
                Console.WriteLine("Welcome to using SeOS!");
                return 1;
            }
            else if (a == 0)
            {
                Console.WriteLine("Password is incorrect!");
                Console.Clear();
                int r = LoginUI();
                return r;
            }
            else 
            {
                return 2;
            }
        }
        static void Main(string[] args)
        {
            Console.Title = "SeoOS v0.1Alpha";
            int result = LoginUI();
            if (result == 1) 
            {
                mainMenu();
            }
            else if (result == 0) //如果登录失败
            {
                int r = LoginUI();
                Console.ReadKey();
            }
            else 
            {
                Console.WriteLine("Internal Error!");
                Console.WriteLine("Please restart the SeOS!");
                Console.WriteLine("Press any key to contiune......");
                Console.ReadKey();
            }
        }
    }
}
